/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.catalyst.marketclearing.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import eu.catalyst.marketclearing.global.DateDeSerializer;
import eu.catalyst.marketclearing.global.DateSerializer;


public class MarketSession implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;
    
    @JsonDeserialize(using = DateDeSerializer.class)
    @JsonSerialize(using = DateSerializer.class)
    private Date sessionStartTime;

    @JsonDeserialize(using = DateDeSerializer.class)
    @JsonSerialize(using = DateSerializer.class)
    private Date sessionEndTime;

    @JsonDeserialize(using = DateDeSerializer.class)
    @JsonSerialize(using = DateSerializer.class)
    private Date deliveryStartTime;

    @JsonDeserialize(using = DateDeSerializer.class)
    @JsonSerialize(using = DateSerializer.class)
    private Date deliveryEndTime;

    private Integer sessionStatusid;

    private Integer marketplaceid;

    private Integer formid;

    private BigDecimal clearingPrice;
    //


    public MarketSession() {
    }

    public MarketSession(Integer id) {
        this.id = id;
    }

    public MarketSession(Integer id, Date sessionStartTime, Date sessionEndTime) {
        this.id = id;
        this.sessionStartTime = sessionStartTime;
        this.sessionEndTime = sessionEndTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getSessionStartTime() {
        return sessionStartTime;
    }

    public void setSessionStartTime(Date sessionStartTime) {
        this.sessionStartTime = sessionStartTime;
    }

    public Date getSessionEndTime() {
        return sessionEndTime;
    }

    public void setSessionEndTime(Date sessionEndTime) {
        this.sessionEndTime = sessionEndTime;
    }

    public Date getDeliveryStartTime() {
        return deliveryStartTime;
    }

    public void setDeliveryStartTime(Date deliveryStartTime) {
        this.deliveryStartTime = deliveryStartTime;
    }

    public Date getDeliveryEndTime() {
        return deliveryEndTime;
    }

    public void setDeliveryEndTime(Date deliveryEndTime) {
        this.deliveryEndTime = deliveryEndTime;
    }

    public Integer getSessionStatusid() {
        return sessionStatusid;
    }

    public void setSessionStatusid(Integer sessionStatusid) {
        this.sessionStatusid = sessionStatusid;
    }

    //Intermediate prototype updates
    public Integer getFormid() {
        return formid;
    }

    public void setFormid(Integer formid) {
        this.formid = formid;
    }

    public BigDecimal getClearingPrice() {
        return clearingPrice;
    }

    public void setClearingPrice(BigDecimal clearingPrice) {
        this.clearingPrice = clearingPrice;
    }

    public Integer getMarketplaceid() {
        return marketplaceid;
    }

    public void setMarketplaceid(Integer marketplaceid) {
        this.marketplaceid = marketplaceid;
    }


    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public boolean equals(Object object) {
        
        if (!(object instanceof MarketSession)) {
            return false;
        }
        MarketSession other = (MarketSession) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }


    public String toString() {
        return "eu.catalyst.marketplace.model.MarketSession[ id=" + id + " ]";
    }

}
