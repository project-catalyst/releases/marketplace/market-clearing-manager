/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.catalyst.marketclearing.model;

import java.io.Serializable;


public class MarketplacehasMarketActor implements Serializable {
    private static final long serialVersionUID = 1L;

    protected MarketplacehasMarketActorPK marketplacehasMarketActorPK;
    private Marketplace marketplace;
    private MarketActor marketActor;

    public MarketplacehasMarketActor() {
    }

    public MarketplacehasMarketActor(MarketplacehasMarketActorPK marketplacehasMarketActorPK) {
        this.marketplacehasMarketActorPK = marketplacehasMarketActorPK;
    }

    public MarketplacehasMarketActor(int id, int marketplaceid, int marketActorid) {
        this.marketplacehasMarketActorPK = new MarketplacehasMarketActorPK(id, marketplaceid, marketActorid);
    }

    public MarketplacehasMarketActorPK getMarketplacehasMarketActorPK() {
        return marketplacehasMarketActorPK;
    }

    public void setMarketplacehasMarketActorPK(MarketplacehasMarketActorPK marketplacehasMarketActorPK) {
        this.marketplacehasMarketActorPK = marketplacehasMarketActorPK;
    }

    public Marketplace getMarketplace() {
        return marketplace;
    }

    public void setMarketplace(Marketplace marketplace) {
        this.marketplace = marketplace;
    }

    public MarketActor getMarketActor() {
        return marketActor;
    }

    public void setMarketActor(MarketActor marketActor) {
        this.marketActor = marketActor;
    }


    public int hashCode() {
        int hash = 0;
        hash += (marketplacehasMarketActorPK != null ? marketplacehasMarketActorPK.hashCode() : 0);
        return hash;
    }


    public boolean equals(Object object) {
        
        if (!(object instanceof MarketplacehasMarketActor)) {
            return false;
        }
        MarketplacehasMarketActor other = (MarketplacehasMarketActor) object;
        if ((this.marketplacehasMarketActorPK == null && other.marketplacehasMarketActorPK != null) || (this.marketplacehasMarketActorPK != null && !this.marketplacehasMarketActorPK.equals(other.marketplacehasMarketActorPK))) {
            return false;
        }
        return true;
    }


    public String toString() {
        return "eu.catalyst.marketplace.model.MarketplacehasMarketActor[ marketplacehasMarketActorPK=" + marketplacehasMarketActorPK + " ]";
    }

}
