/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.catalyst.marketclearing.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;


@Entity
@Table(name = "MarketActionCounterOffer")
@XmlRootElement
public class MarketActionCounterOffer implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    private Integer marketAction_Bid_id;
    private Integer marketAction_Offer_id;
    private BigDecimal exchangedValue;
    public MarketActionCounterOffer() {
    }

    public MarketActionCounterOffer(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    //Intermediate prototype updates
    public Integer getMarketAction_Bid_id() {
        return marketAction_Bid_id;
    }

    public void setMarketAction_Bid_id(Integer marketAction_Bid_id) {
        this.marketAction_Bid_id = marketAction_Bid_id;
    }

    public Integer getMarketAction_Offer_id() {
        return marketAction_Offer_id;
    }

    public void setMarketAction_Offer_id(Integer marketAction_Offer_id) {
        this.marketAction_Offer_id = marketAction_Offer_id;
    }

    public BigDecimal getExchangedValue() {
        return exchangedValue;
    }

    public void setExchangedValue(BigDecimal exchangedValue) {
        this.exchangedValue = exchangedValue;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof MarketActionCounterOffer)) {
            return false;
        }
        MarketActionCounterOffer other = (MarketActionCounterOffer) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.geyser.marketplace.model.MarketActionCounterOffer[ id=" + id + " ]";
    }

}
