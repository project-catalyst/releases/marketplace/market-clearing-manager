package eu.catalyst.marketclearing.service;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eu.catalyst.marketclearing.model.MarketAction;
import eu.catalyst.marketclearing.model.MarketActionCounterOffer;
import eu.catalyst.marketclearing.global.MyPayload;;

@Stateless
@Path("/marketplace/{marketId}/form/{form}/marketsessions/{sessionId}/actions/sorted/")
public class EnergyMarketClearing {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(EnergyMarketClearing.class);

	static HashSet<String> setOfFormsToClear = new HashSet<>();

	static {
		setOfFormsToClear.add("electric_energy");
		setOfFormsToClear.add("thermal_energy");
		setOfFormsToClear.add("it_load");
	}

	public EnergyMarketClearing() {
	}

	@POST
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public List<MarketAction> postListActionsToClearing(@PathParam("marketId") int marketId, @PathParam("form") String form,
			@PathParam("sessionId") int sessionId, List<MarketAction> actions) {

		System.out.println("MarketEnergyClearing");

		if (setOfFormsToClear.contains(form)) {

			ArrayList<myMarketAction> bidsActionList = new ArrayList<myMarketAction>();
			ArrayList<myMarketAction> offersActionList = new ArrayList<myMarketAction>();
			ArrayList<MarketActionCounterOffer> marketActionCounterOfferList = new ArrayList<MarketActionCounterOffer>();
			for (Iterator<MarketAction> iterator = actions.iterator(); iterator.hasNext();) {

				// Filtering MarketAction by Status ("valid") and Splitting by ActionStatus
				// ("bid"/"offer")
				MarketAction market = iterator.next();
				//if (market.getActionStatusid().getId() == 2) { // 2== "valid"
				if (market.getStatusid() == 2) { // 2== "valid"
					myMarketAction myAction = new myMarketAction();
					myAction.marketAction = market;
					//myAction.marketAction.getActionStatusid().setId(9);
					//myAction.marketAction.getActionStatusid().setStatus("rejected");
					myAction.marketAction.setStatusid(10);
					myAction.residualValue = market.getValue();
					//if (market.getActionTypeid().getId() == 1) { // 1== "bid"
					if (market.getActionTypeid() == 2) { // 2== "bid" new
						bidsActionList.add(myAction);
					} else { // 1== "offer" new
						offersActionList.add(myAction);
					}
				}

			}
			// Sorting bidsActionList by price in descending order
			Comparator<myMarketAction> byPriceDesc = new Comparator<myMarketAction>() {
				public int compare(myMarketAction left, myMarketAction right) {
					// return Long.compare(right.marketAction.getPrice(),
					// left.marketAction.getPrice());
					return right.marketAction.getPrice().compareTo(left.marketAction.getPrice());
				}
			};
			Collections.sort(bidsActionList, byPriceDesc);

			// Sorting offersActionList by price in ascending order
			Comparator<myMarketAction> byPriceAsc = new Comparator<myMarketAction>() {
				public int compare(myMarketAction left, myMarketAction right) {
//	        	            return Long.compare(left.marketAction.getPrice(),
//	        	                    right.marketAction.getPrice());
					return left.marketAction.getPrice().compareTo(right.marketAction.getPrice());
				}
			};
			Collections.sort(offersActionList, byPriceAsc);

			// last BID.Price = null;
			// flag terminate LOOPS = false;
			// clearing price := null;

			BigDecimal newClearingPrice = null;
			BigDecimal lastBidPrice = null;
			boolean executeLoop = true;

			Iterator<myMarketAction> iteratorBids = bidsActionList.iterator();
			myMarketAction currentBid = null;
			myMarketAction precBid = null;
			if (iteratorBids.hasNext()) {
				currentBid = iteratorBids.next();
			} else {
				executeLoop = false;
			}
			Iterator<myMarketAction> iteratorOffers = offersActionList.iterator();
			myMarketAction currentOffer = null;
			myMarketAction precOffer = null;
         			if (iteratorOffers.hasNext()) {
				currentOffer = iteratorOffers.next();
			} else {
				executeLoop = false;
			}

			if (executeLoop && currentBid.marketAction.getPrice().compareTo(currentOffer.marketAction.getPrice()) < 0) {
				executeLoop = false;
			}

			while (executeLoop) {

				// If BIDi.price < OFFERj.price
				// Compute clearing price := OFFERj.Price
				// flag Terminate LOOPS := true

				if (currentBid.marketAction.getPrice().compareTo(currentOffer.marketAction.getPrice()) < 0) {
					if (currentOffer.marketAction.getPrice().compareTo(precOffer.marketAction.getPrice()) == 0) {
						newClearingPrice = currentOffer.marketAction.getPrice();
					} else if ((currentBid.marketAction.getPrice().compareTo(precBid.marketAction.getPrice()) == 0)) {
						newClearingPrice = currentBid.marketAction.getPrice();
					} else { // intersection as interval
						newClearingPrice = precOffer.marketAction.getPrice();
					}
					break;
				}

				// If BIDi.residualValue == OFFERj.residualValue
				// last BID.Price = BIDi.Price
				// Execute Acceptance(BIDi):
				// Insert item in marketActionCounterOfferList (BIDi, OFFERj,
				// OFFERj.residualValue)
				// BIDi.residualValue := zero
				// BIDi.status := "accepted" (7)
				// next iteratorBids
				// Execute Acceptance(OFFERj):
				// OFFERj.residualValue := zero
				// OFFERj.status := "accepted" (7)
				// next iteratorOffers

//            if (currentBid.residualValue == currentOffer.residualValue) {
				if (currentBid.residualValue.equals(currentOffer.residualValue)) {

					MarketActionCounterOffer myMarketActionCounterOffer = new MarketActionCounterOffer();

//					myMarketActionCounterOffer.setMarketAction_Bid_id(currentBid.marketAction);
//					myMarketActionCounterOffer.setMarketAction_Offer_id(currentOffer.marketAction);
  					myMarketActionCounterOffer.setMarketAction_Bid_id(currentBid.marketAction.getId());
					myMarketActionCounterOffer.setMarketAction_Offer_id(currentOffer.marketAction.getId());
					myMarketActionCounterOffer.setExchangedValue(currentOffer.residualValue);
					marketActionCounterOfferList.add(myMarketActionCounterOffer);

					// currentBid.residualValue. = 0L;
					currentBid.residualValue = new BigDecimal(0);
//					currentBid.marketAction.getActionStatusid().setId(7);
//					currentBid.marketAction.getActionStatusid().setStatus("accepted");
					currentBid.marketAction.setStatusid(7);



					// currentOffer.residualValue = 0L;
					currentOffer.residualValue = new BigDecimal(0);
//					currentOffer.marketAction.getActionStatusid().setId(7);
//					currentOffer.marketAction.getActionStatusid().setStatus("accepted");
					currentOffer.marketAction.setStatusid(7);

					if (iteratorBids.hasNext()) {
						precBid = currentBid;
						currentBid = iteratorBids.next();
					} else {
						newClearingPrice = currentOffer.marketAction.getPrice();
						break;
					}

					if (iteratorOffers.hasNext()) {
						precOffer = currentOffer;
						currentOffer = iteratorOffers.next();
						continue;
					} else {
						newClearingPrice = currentOffer.marketAction.getPrice();
						break;
					}
				}

				// If BIDi.residualValue < OFFERj.residualValue
				// last BID.Price = BIDi.Price
				// Execute Partially_Acceptance(OFFERj):
				// OFFERj.residualValue := OFFERj.residualValue - BIDi.residualValue
				// OFFERj.status := "partially_accepted" (8)
				// Execute Acceptance(BIDi):
				// Insert item in marketActionCounterOfferList (BIDi, OFFERj,
				// BIDi.residualValue)
				// BIDi.residualValue := zero
				// BIDi.status := "accepted" (7)
				// next iteratorBids
				//

				// if (currentBid.residualValue < currentOffer.residualValue) {
				if (currentBid.residualValue.compareTo(currentOffer.residualValue) < 0) {

					// currentOffer.residualValue -= currentBid.residualValue;
					currentOffer.residualValue = currentOffer.residualValue.subtract(currentBid.residualValue);

//					currentOffer.marketAction.getActionStatusid().setId(8);
//					currentOffer.marketAction.getActionStatusid().setStatus("partially_accepted");
					currentOffer.marketAction.setStatusid(9);

					MarketActionCounterOffer myMarketActionCounterOffer = new MarketActionCounterOffer();

//					myMarketActionCounterOffer.setMarketAction_Bid_id(currentBid.marketAction);
//					myMarketActionCounterOffer.setMarketAction_Offer_id(currentOffer.marketAction);
					myMarketActionCounterOffer.setMarketAction_Bid_id(currentBid.marketAction.getId());
					myMarketActionCounterOffer.setMarketAction_Offer_id(currentOffer.marketAction.getId());

					myMarketActionCounterOffer.setExchangedValue(currentBid.residualValue);
					marketActionCounterOfferList.add(myMarketActionCounterOffer);

//              currentBid.residualValue = 0L;
					currentBid.residualValue = new BigDecimal(0);
//					currentBid.marketAction.getActionStatusid().setId(7);
//					currentBid.marketAction.getActionStatusid().setStatus("accepted");
					currentBid.marketAction.setStatusid(7);

					if (iteratorBids.hasNext()) {
						precOffer = currentOffer;
						precBid = currentBid;
						currentBid = iteratorBids.next();
						continue;
					} else {
						newClearingPrice = currentOffer.marketAction.getPrice();
						break;
					}

				}

				// If BIDi.residualValue > OFFERj.residualValue
				// last BID.Price = BIDi.Price
				// Execute Partially_Acceptance(BIDi):
				// Insert item in marketActionCounterOfferList (BIDi, OFFERj,
				// OFFERj.residualValue)
				// BIDi.residualValue := BIDi.residualValue - OFFERj.residualValue
				// BIDi.status := "partially_accepted" (8)
				// Execute Acceptance(OFFERj):
				// OFFERj.residualValue := zero
				// OFFERj.status := "accepted" (7)
				// next iteratorOffers

//          if (currentBid.residualValue > currentOffer.residualValue) {
				if (currentBid.residualValue.compareTo(currentOffer.residualValue) > 0) {

					MarketActionCounterOffer myMarketActionCounterOffer = new MarketActionCounterOffer();

//					myMarketActionCounterOffer.setMarketAction_Bid_id(currentBid.marketAction);
//					myMarketActionCounterOffer.setMarketAction_Offer_id(currentOffer.marketAction);
					myMarketActionCounterOffer.setMarketAction_Bid_id(currentBid.marketAction.getId());
					myMarketActionCounterOffer.setMarketAction_Offer_id(currentOffer.marketAction.getId());

					myMarketActionCounterOffer.setExchangedValue(currentOffer.residualValue);
					marketActionCounterOfferList.add(myMarketActionCounterOffer);

					// currentBid.residualValue -= currentOffer.residualValue;
					currentBid.residualValue = currentBid.residualValue.subtract(currentOffer.residualValue);
//					currentBid.marketAction.getActionStatusid().setId(8);
//					currentBid.marketAction.getActionStatusid().setStatus("partially_accepted");
					currentBid.marketAction.setStatusid(9);

					// currentOffer.residualValue = 0L;
					currentOffer.residualValue = new BigDecimal(0);
//					currentOffer.marketAction.getActionStatusid().setId(7);
//					currentOffer.marketAction.getActionStatusid().setStatus("accepted");
					currentOffer.marketAction.setStatusid(7);

					if (iteratorOffers.hasNext()) {
						precBid = currentBid;
						precOffer = currentOffer;
						currentOffer = iteratorOffers.next();
					} else {
						newClearingPrice = currentBid.marketAction.getPrice();
						break;
					}
				}
			}

			// Merging Bids and Offers
			ArrayList<MarketAction> bidsOffersActionList = new ArrayList<MarketAction>();
			for (iteratorBids = bidsActionList.iterator(); iteratorBids.hasNext();) {
				myMarketAction mymarket = iteratorBids.next();
				bidsOffersActionList.add(mymarket.marketAction);
			}
			for (iteratorOffers = offersActionList.iterator(); iteratorOffers.hasNext();) {
				myMarketAction mymarket = iteratorOffers.next();
				bidsOffersActionList.add(mymarket.marketAction);
			}

			// Invoke Rest Service (POST)
			// "/marketsessionmanager/marketplace/{marketId}/form/{form}/marketsessions/{sessionId}/billing/actions/"
			// of Component MarketSessionManager
			// passing bidsOffersActionList, and...

			
			
			if (!bidsOffersActionList.isEmpty()) {
			
				//force actions to return bidsOffersActionList
				actions = bidsOffersActionList;
				
				MyPayload myPayload = new MyPayload();
				myPayload.setMarketActionCounterOfferList(marketActionCounterOfferList);
				myPayload.setBidsOffersActionList(bidsOffersActionList);
				myPayload.setNewClearingPrice(newClearingPrice);
				
				try {
					Properties prop = new Properties();

					prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));

					// Check the property informationBrokerServerUrl in file config.properties

					String marketSessionManagerUrl = prop.getProperty("marketSessionManagerUrl");

					URL url = new URL(marketSessionManagerUrl + "/marketplace/" + marketId
							+ "/form/" + form + "/marketsessions/" + sessionId + "/billing/actions/");

					System.out.println("Rest url: " + url.toString());

					HttpURLConnection connService = (HttpURLConnection) url.openConnection();
					connService.setDoOutput(true);
					connService.setRequestMethod("POST");
					connService.setRequestProperty("Accept", "application/json");
					connService.setRequestProperty("Content-Type", "application/json");
					GsonBuilder gb = new GsonBuilder();

					// gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
					gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
					Gson gson = gb.create();

					String record = gson.toJson(myPayload);

					System.out.println("Rest Request: " + record);

					OutputStream os = connService.getOutputStream();
					os.write(record.getBytes());

					int myResponseCode = connService.getResponseCode();
					System.out.println("HTTP error code :" + myResponseCode);

					os.flush();
					os.close();
					connService.disconnect();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
		return actions;
	}

	static class myMarketAction {
		MarketAction marketAction;
		// long residualValue;
		BigDecimal residualValue;

		public myMarketAction() {
		}
	}

}


